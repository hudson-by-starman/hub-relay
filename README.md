This project stores any files needed to be installed on a relay device to make it fucntional.


The Relay device plugs into a wall and allows the user to plug a device into it, either a heater or a light. It is an ESP8266 controlling a relay to switch a device on or off.  It connects to the Hub device over SSL. The encryption key is stored on the ESP8266 so the connection is secure and resistant to MITM attacks. The connection is also done over the Hubs own AP network for extra security. It uses authenticated MQTT messages between relay and hub device. When the user selects to turn a device on or off, a websocket message is sent to the server which runs a python script to send an MQTT message to any applicable relay devices which are listening, these relay devices switch on or off depending on the message.

